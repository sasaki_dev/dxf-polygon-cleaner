[ ![Codeship Status for sasaki_dev/dxf-polygon-cleaner](https://www.codeship.io/projects/3e11b3e0-e362-0131-5422-0a909c523ff1/status)](https://www.codeship.io/projects/25433)

# DXF Polygon Cleaner

Takes the result table of [dxf-polygon-importer][2] and runs cleaning and validation functions on a given file, project and version combination.

The initial version was developed to solve following geometry problems:

![Geometry problems](https://bytebucket.org/sasaki_dev/dxf-polygon-cleaner/raw/4569d3f89d47cf325805f51c76e47f4a7cd3d015/geometry-problems.png)

Key for yellow numbered test problem sets:

1. Overlapping polygons and duplicate labels
2. Multiple labels
3. Split label ('0', '002')
4. C shaped polygon
5. C shaped polygon with empty core
6. Self-intersecting geometry with duplicate label
7. Doughnut geometry
8. Doughnut geometry with label only in core (should be in ring)
9. '009A' appears as polygon, but is only enclosed by line on the left
10. 2 Overlapping polygons
11. 3 Overlapping polygons (one that contains both)
12. Doughnut with label in core and ring
13. Orphaned label
14. Non-compact shape type without label


## Installation and requirements

    npm install dxf-polygon-cleaner

The module **requires** a [PostGIS][1] enabled PostgreSQL database (version 9.3 or higher) and expects a database structure created by the [dxf-polygon-importer][2].

Following environment variables need to be set:

    DXF_TOOLS_DB_HOST              ...  PostgreSQL server host
    DXF_TOOLS_DB_PORT              ...  PostgreSQL server port
    DXF_TOOLS_DB_USER              ...  Database user name
    DXF_TOOLS_DB_PASSWORD          ...  Database user password
    DXF_TOOLS_DB_NAME              ...  Database name

*Sensible database connection defaults are applied for typical 'localhost' environments, a database-name is required however.*

The module will create a table `approved_dxf_features`, for permantly storing a single version of approved features, and 2 temporary views, for accessing the latest versions per feature type, project and filename.

## Usage

    var Cleaner = require('dxf-polygon-cleaner');

    // Cleaner(filename, project, version);
    var cleaner = Cleaner('mydrawing.dxf', 'myproject', 1);

    cleaner.setup().then(function () {

      // data cleaning and/or validation methods

    }).then(function () {
      return cleaner.teardown();
    });

### Run tests

  Requires a `test` database.

    $ npm test

## API

### initialize `Cleaner(filename, project, version)`

The class takes 3 arguments to identify the feature collection that should be processed: the `filename` of the imported dxf file, the `project` identifier and the imported `version`.

### setup `Cleaner.setup()`

Creates temporary views to make succeeding database queries and operations a little easier.

### teardown `Cleaner.teardown()`

Removes temporary views and ends the database connection pool.

### fixDoughnuts `Cleaner.fixDoughnuts()`

Creates doughnut geometries and removes overlaps of polygons and their completely contained polygons.

### deleteEmptyPolygons `Cleaner.deleteEmptyPolygons()`

Deletes all polygons with an area smaller than 1 [square-unit].

### labelPolygons `Cleaner.labelPolygons()`

Assigns labels as names to enclosing polygons.

### fixDoughnutLabels `Cleaner.fixDoughnutLabels()`

Moves labels from cores to non-labeled rings of doughnut geometries.

### flagPolygons `Cleaner.flagPolygons()`

Adds following validation flags to all polygons:

`hasLabel, hasManyLabels, hasSharedLabels`  
`compactness, area`  
`isOverlapping, overlappingPolygons`  
`isValid, validReason`

The *compactness* indicator measures shape form and is calculated by the following formula:

![compactness = (Area / Perimeter2) * 4 * Pi](http://latex.codecogs.com/gif.latex?compactness=\frac{A}{P^{2}}%20\cdot%204%20\cdot%20\pi)  

Example values:

* cirlce: 1
* square: ~0.8
* long shaped corridor: ~0.15

### flagLabels `Cleaner.flagLabels()`

Adds following validation flags to all label points:

`isInPolygon, isInManyPolygons`

### approvePolygons `Cleaner.approvePolygons(options)`

Inserts a copy of the currently processed features into `approved_dxf_features`, and deletes any prior existing versions in that table.

    {
      endPool:   true|false  end database pool when done
    }

### getInvalidGeometryDetails `Cleaner.getInvalidGeometryDetails()`

Return a GeoJSON object with point geometries and meta information of possibly problematic geometries, like self-intersections.

### getGeoJson `Cleaner.getGeoJson(options)`

Return a GeoJSON Feature collection of the currently processed features. It takes and `options` object with following properties:

    {
      approved:  true|false  return only approved features
      endPool:   true|false  end database pool when done
    }

### setVersion `Cleaner.setVersion()`

Finds the most recent version of a project and filename combination and sets the `Cleaner._version` property accordingly. Useful when defaulting to the last version.

### fullService `Cleaner.fullService()`

Alias for running common tasks:

  1. fixDoughnuts
  2. deleteEmptyPolygons
  3. labelPolygons
  4. fixDoughnutLabels
  5. flagPolygons
  6. flagLabels
  7. getGeoJson

Returns GeoJSON.


[1]: http://postgis.net/
[2]: https://bitbucket.org/sasaki_dev/dxf-polygon-importer
