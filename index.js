var Knex = require('knex'),
    _ = require('lodash'),
    RSVP = require('rsvp'),
    config = require('./config');



/**
 * Instantiate class to e, execute automatic cleanup routines (fixing doughnut geometries, etc.) and flagging of problematic geometries.
 * @param {String} filename Filenname
 * @param {String} project  Project identifier
 * @param {Number} version  Version to check
 */
function Cleaner (filename, project, version) {

  if (!(this instanceof Cleaner)) return new Cleaner (filename, project, version);


  this._dbClient = Knex({
    client: 'pg',
    connection: {
      host: config.host,
      port: config.port,
      user: config.user,
      password: config.password,
      database: config.database
    }
  });
  this._dbViews = {};

  this._filename = filename;
  this._project = project;
  this._version = version;
  this._featureTypes = ['polygon', 'label'];

  return this;
}



/**
 * Create temporary db views with valid geometries, makes Cleaner queries easier and safer
 */
Cleaner.prototype.setup = function () {
  var that = this,
      promises;

  promises = _.map(that._featureTypes, function (ft) {
    var cleanFilename = that._filename.slice(0, that._filename.length-4).toLowerCase().replace(' ','_');

    that._dbViews[ft] = that._project + '_' + cleanFilename + '_' + that._version + '_' + ft;

    return that._dbClient.raw('CREATE OR REPLACE VIEW ' + that._dbViews[ft] + ' AS SELECT * FROM dxf_features WHERE ST_IsValid(geom) AND version = ' + that._version + ' AND type = \'' + ft + '\' AND filename=\'' + that._filename + '\'');
  });

  return RSVP.all(promises)
  .then(function () {
    return that;
  });
};



/**
 * Remove temporary DB views
 */
Cleaner.prototype.teardown = function () {
  var that = this,
      promises;

  promises = _.map(that._featureTypes, function (ft) {
    return that._dbClient.raw('DROP VIEW IF EXISTS ' + that._dbViews[ft]);
  });

  return RSVP.all(promises)
  .then(function () {
    that._dbClient.destroy();
    return that;
  });
};



/**
 * Cut doughnuts out of overlapping polygons
 */
Cleaner.prototype.fixDoughnuts = function () {
  var that = this;

  return that._dbClient.raw('SELECT o.id AS o_id, o.validations AS o_validations, i.id AS i_id FROM ' + that._dbViews.polygon + ' AS o, ' + that._dbViews.polygon + ' AS i WHERE o.id != i.id AND ST_Contains(ST_Buffer(o.geom, 0.1), i.geom)')

  .then(function (data) {
    var promises, doughnuts;

    doughnuts = _.groupBy(data.rows, 'o_id');
    promises = _.map(data.rows, function (row) {
      row.o_validations.isDoughnutRing = true;
      row.o_validations.doughnutCores = _.map(doughnuts[row.o_id], function (d) {
        return d.i_id;
      });

      return that._dbClient.raw('WITH i AS (SELECT id, geom FROM ' + that._dbViews.polygon + ' WHERE id=' + row.i_id + ') UPDATE dxf_features SET geom = ST_Difference(geom, (SELECT geom FROM i)), validations = \'' + JSON.stringify(row.o_validations) + '\' WHERE id = ' + row.o_id);
    });

    return RSVP.all(promises);
  })

  .then(function () {
    return that;
  });
};



/**
 * Deletes all polygons with an area smalle than 1 [square unit]
 */
Cleaner.prototype.deleteEmptyPolygons = function () {
  var that = this;

  return that._dbClient('dxf_features')
  .whereRaw('ST_Area(geom) <= 1')
  .andWhere({
    type: 'polygon',
    project: that._project,
    version: that._version,
    filename: that._filename
  })
  .del()

  .then(function () {
    return that;
  });
};



/**
 * Assign labels to enclosing polygons
 */
Cleaner.prototype.labelPolygons = function () {
  var that = this;

  return this._dbClient.raw('SELECT polygons.id AS polygon_id, polygons.validations, labels.validations->>\'text\' AS polygon_name FROM ' + that._dbViews.polygon + ' AS polygons LEFT JOIN ' + that._dbViews.label + ' AS labels ON ST_Contains(polygons.geom, labels.geom) OR ST_Contains(ST_Buffer(polygons.geom, 0.1), labels.geom)')

  .then(function (data) {
    var promises = [];

    _.forOwn(_.groupBy(data.rows, 'polygon_id'), function (polygons, polygon_id) {
      // safe to assume that all polygon properties are identical,
      // except for the name, therefore it's safe to take the first one
      var validations = polygons[0].validations;

      validations.names = _.uniq(_.compact(_.pluck(polygons, 'polygon_name')));

      promises.push(
        that._dbClient('dxf_features')
        .where('id', '=', polygon_id)
        .update({
          name: validations.names.join(',') || null,
          validations: validations
        })
      );
    });

    return RSVP.all(promises);
  })

  .then(function () {
    return that;
  });
};



/**
 * Moves labels from cores to non-labeled rings of doughnut geometries.
 */
Cleaner.prototype.fixDoughnutLabels = function () {
  var that = this;

  return that._dbClient.raw('SELECT r.id AS r_id, r.validations AS r_validations, c.id AS c_id, c.validations AS c_validations FROM ' + that._dbViews.polygon + ' AS r, json_array_elements(r.validations->\'doughnutCores\') AS core JOIN ' + that._dbViews.polygon + ' AS c ON c.id::text=core::text WHERE r.validations->>\'isDoughnutRing\'=\'true\' AND r.name IS NULL AND c.name IS NOT NULL')

  .then(function (data) {
    var promises = [];

    _.forEach(data.rows, function (row) {

      row.r_validations.names = _.union(row.r_validations.names, row.c_validations.names) || [];
      row.c_validations.names = [];

      promises.push(
        that._dbClient('dxf_features')
        .where('id', '=', row.r_id)
        .update({
          name: row.r_validations.names.join(',') || null,
          validations: row.r_validations
        })
      );

      promises.push(
        that._dbClient('dxf_features')
        .where('id', '=', row.c_id)
        .update({
          name: null,
          validations: row.c_validations
        })
      );
    });

    return RSVP.all(promises);
  })

  .then(function () {
    return that;
  });
};



/**
 * Add validation flags to all polygons.
 */
Cleaner.prototype.flagPolygons = function () {
  var that = this;

  return that._dbClient.raw('WITH o AS ( SELECT a.id AS o_id, json_agg(b.id) AS overlapping_ids, True AS is_overlapping FROM ' + that._dbViews.polygon + ' AS a, ' + that._dbViews.polygon + ' AS b WHERE ST_Overlaps(a.geom, b.geom) GROUP BY a.id ) SELECT id, validations, ST_Perimeter(geom) AS p, ST_Area(geom) AS a, is_overlapping, overlapping_ids FROM ' + that._dbViews.polygon + ' AS r LEFT JOIN o ON id = o_id')

  .then(function (data) {
    var appliedLabels,
        promises = [];

    appliedLabels = _.flatten(data.rows, function (row) {
      return row.validations.names;
    });

    _.forEach(data.rows, function (row) {
      var sharedLabelCheck;

      row.validations.hasLabel = (row.validations.names.length > 0) ? true : false;
      row.validations.hasManyLabels = (row.validations.names.length > 1) ? true : false;

      sharedLabelCheck = _.map(row.validations.names, function (name) {
        var found = _.filter(appliedLabels, function (al) {
          return al === name;
        });
        return found.length > 1;
      });
      row.validations.hasSharedLabels = _.contains(sharedLabelCheck, true);

      row.validations.compactness = (row.a / Math.pow(row.p, 2)) * 4 * Math.PI;
      row.validations.area = row.a;

      row.validations.isOverlapping = row.is_overlapping ? true : false;
      row.validations.overlappingPolygons = row.overlapping_ids || [];

      promises.push(
        that._dbClient('dxf_features')
        .where('id', '=', row.id)
        .update('validations', row.validations)
      );
    });

    return RSVP.all(promises);
  })

  // select and flag invalid geometries
  .then(function () {
    return that._dbClient.raw('SELECT id, validations, valid(ST_IsValidDetail(geom)) AS is_valid, reason(ST_IsValidDetail(geom)) AS valid_reason FROM dxf_features WHERE ST_IsValid(geom) = FALSE AND type = \'polygon\' AND project = \'' + that._project + '\' AND filename = \'' + that._filename + '\' AND version = ' + that._version);
  })

  .then(function (data) {
    var promises;

    promises = _.map(data.rows, function (row) {
      row.validations.isValid = row.is_valid;
      row.validations.validReason = row.valid_reason;
      return that._dbClient('dxf_features')
      .where('id', '=', row.id)
      .update('validations', row.validations);
    });

    return RSVP.all(promises);
  })

  .then(function () {
    return that;
  });
};



/**
 * Add validtion flags to all label points.
 */
Cleaner.prototype.flagLabels = function () {
  var that = this;

  return that._dbClient(that._dbViews.polygon)
  .columns('validations')
  .whereNotNull('name')

  .then(function (rows) {
    var polygonLabels = _.flatten(rows, function (row) {
      return row.validations.names;
    });

    return that._dbClient(that._dbViews.label)
    .columns('id', 'validations')

    .then(function (rows) {
      var promises = _.map(rows, function (row) {
        var labelInPolygons = _.filter(polygonLabels, function (l) {
          return l === row.validations.text;
        });

        row.validations.isInPolygon = (labelInPolygons.length > 0);
        row.validations.isInManyPolygons = (labelInPolygons.length > 1);

        return that._dbClient('dxf_features')
          .where('id', '=', row.id)
          .update({
            name: row.validations.text || null,
            validations: row.validations
          });
      });

      return RSVP.all(promises);
    });
  })

  .then(function () {
    return that;
  });
};



/**
 * Inserts a copy of valid and named features into `approved_dxf_features`
 * and deletes any prior existing versions in that table.
 * @param {Object} options Configuration object
 *                         {
 *                           endPool  {boolean}  end DB pool
 *                         }
 */
Cleaner.prototype.approvePolygons = function (options) {
  var that = this;

  return that._dbClient.schema.hasTable('approved_dxf_features')

  .then(function (exists) {
    if (!exists) {
      return that._dbClient.schema.createTable('approved_dxf_features', function (t) {
        t.increments('id').primary();
        t.string('name');
        t.string('filename');
        t.string('project');
        t.json('validations');
        t.specificType('geom', 'GEOMETRY');
      });
    }
  })

  .then(function () {
    return that._dbClient('approved_dxf_features')
      .where({
        'project': that._project,
        'filename': that._filename
      })
      .del();
  })

  .then(function () {
    return that._dbClient.raw('INSERT INTO approved_dxf_features (name, filename, project, validations, geom) SELECT name, filename, project, validations, geom FROM dxf_features WHERE ST_IsValid(geom) AND type = \'polygon\' AND project = \'' + that._project + '\' AND filename = \'' + that._filename + '\' AND version = ' + that._version + 'AND name IS NOT NULL;');
  })

  .then(function () {
    // end DB pool
    if (options && options.destroyDbClient) that._dbClient.destroy();
    return that;
  });
};



/**
 * Return a GeoJSON object with point geometries and meta information of
 * possibly problematic geometries, like self-intersections.
 */
Cleaner.prototype.getInvalidGeometryDetails = function () {
  var that = this;

  return that._dbClient.raw('SELECT row_to_json(fc) AS geojson FROM (SELECT \'FeatureCollection\' AS type, array_to_json(array_agg(f)) AS features FROM (SELECT \'Feature\' AS type, rg.id AS id, ST_AsGeoJSON(ST_Transform(ST_SetSRID(location(ST_IsValidDetail(geom)), 3857), 4326))::json AS geometry,  row_to_json((SELECT r FROM (SELECT name, reason(ST_IsValidDetail(geom))) AS r  )) AS properties FROM dxf_features AS rg WHERE ST_IsValid(geom) = FALSE AND type = \'polygon\' AND project = \'' + that._project + '\' AND filename = \'' + that._filename + '\' AND version = ' + that._version + ') AS f) AS fc limit 1;')

  .then(function (data) {
    return data.rows[0].geojson;
  });
};



/**
 * Return a GeoJSON Feature collection of the currently processed features.
 * @param {Object}   options Configuration object
 *                         {
 *                           approved {boolean}  only approved features
 *                           endPool  {boolean}  end DB pool
 *                         }
 * @return {Promise}         Resolves into GeoJSON
 */
Cleaner.prototype.getGeoJson = function (options) {
  var that = this,
      query, version;

  if (options && options.approved) {

    query = 'SELECT row_to_json(fc) AS geojson FROM (SELECT \'FeatureCollection\' AS type, array_to_json(array_agg(f)) AS features FROM (SELECT \'Feature\' AS type, rg.id AS id, ST_AsGeoJSON(ST_Transform(rg.geom,4326))::json AS geometry, row_to_json((SELECT r FROM (SELECT name, filename, project, validations) AS r)) AS properties FROM approved_dxf_features AS rg WHERE project = \'' + that._project + '\' AND filename = \'' + that._filename + '\' ) AS f  ) AS fc limit 1;';

    version = 'approved';

  } else {

    query = 'SELECT row_to_json(fc) AS geojson FROM (SELECT \'FeatureCollection\' AS type, array_to_json(array_agg(f)) AS features FROM (SELECT \'Feature\' AS type, rg.id AS id, ST_AsGeoJSON(ST_Transform(rg.geom, 4326))::json AS geometry, row_to_json((SELECT r FROM (SELECT name, filename, version, project, validations) AS r)) AS properties FROM dxf_features AS rg WHERE version = ' + that._version  + ' AND project = \'' + that._project + '\' AND filename = \'' + that._filename + '\' ) AS f  ) AS fc limit 1;';

  }

  return that._dbClient.raw(query)

  .then(function (data) {
    var geojson = data.rows[0].geojson;

    geojson.meta = {
      project: that._project,
      filename: that._filename,
      version: that._version
    };

    // end DB pool
    if (options && options.endPool) that._dbClient.destroy();

    return geojson;
  });
};



/**
 * Sets version property to latest version number
 */
Cleaner.prototype.setVersion = function () {
  var that = this;

  return that._dbClient('dxf_features')
    .where({
      'filename': that._filename,
      'project': that._project
    })
    .max('version as version')

  .then(function (rows) {
    that._version = rows[0].version;
    return that;
  });
};



/**
 * Shortcut for a combination of commonly used tasks
 * @return {Promise} Resolves into GeoJSON
 */
Cleaner.prototype.fullService = function () {
  var that = this;

  return that.setup()
  .then(function () {
    return that.fixDoughnuts();
  })
  .then(function () {
    return that.deleteEmptyPolygons();
  })
  .then(function () {
    return that.labelPolygons();
  })
  .then(function () {
    return that.fixDoughnutLabels();
  })
  .then(function () {
    return that.flagPolygons();
  })
  .then(function () {
    return that.flagLabels();
  })
  .then(function () {
    return that.getGeoJson();
  }).then(function (geojson) {
    return geojson;
  }).finally(function () {
    that.teardown();
  });
};



/**
 * Attempt to fix invalid geometries
 * TODO: convert multi to single polygons
 */
Cleaner.prototype.makeValid = function () {
  var that = this;

  return that._dbClient.raw('UPDATE dxf_features SET geom = ST_MakeValid(geom) WHERE ST_IsValid(geom) = FALSE AND type = \'polygon\' AND project = \'' + that._project + '\' AND filename = \'' + that._filename + '\' AND version = ' + that._version)

  .then(function (data) {
    return data;
  });
};


module.exports = Cleaner;
