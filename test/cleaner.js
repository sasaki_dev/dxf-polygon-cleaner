var Cleaner = require('../.'),
    config = require('../config'),
    test = require('tape'),
    fs = require('fs'),
    _ = require('lodash'),
    Knex = require('knex');

var dbClient, sqlStream;


dbClient = Knex({
  client: 'pg',
  connection: {
    host: config.host,
    port: config.port,
    user: config.user,
    password: config.password,
    database: config.database
  }
});


function importDataFixture (sql) {
  dbClient.raw(sql)
  .then(function () {

    // data import done, run tests
    test('cleaner API', function (t) {
      var cleaner;

      t.plan(47);

      cleaner = Cleaner('test.dxf', 'test', 1);

      // Basic API existence
      t.equal(typeof cleaner, 'object', 'Cleaner is OK');

      // setup
      t.equal(typeof cleaner.setup, 'function', 'Cleaner.setup() exists');
      cleaner.setup().then(function () {
        return dbClient.schema.hasTable('test_test_1_polygon');
      }).then(function (exists) {
        t.ok(exists, 'temporary polygon view created');
        return dbClient.schema.hasTable('test_test_1_label');
      }).then(function (exists) {
        t.ok(exists, 'temporary label view created');
      })

      // getGeoJson
      .then(function () {
        t.equal(typeof cleaner.getGeoJson, 'function', 'Cleaner.getGeoJson() exists');
        return cleaner.getGeoJson();
      }).then(function (data) {
        t.equal(data.type, 'FeatureCollection', 'Cleaner.getGeoJson is OK');
        t.equal(data.features.length, 44, 'geojson features OK');
      })

      // fixDoughnuts
      .then(function () {
        t.equal(typeof cleaner.fixDoughnuts, 'function', 'Cleaner.fixDoughnuts() exists');
        return cleaner.fixDoughnuts();
      }).then(function () {
        return dbClient.raw('SELECT f2.id FROM dxf_features as f1, dxf_features as f2  WHERE ST_Overlaps(f1.geom, f2.geom) AND f2.type = \'polygon\' AND f1.id = 15');
      }).then(function (rows) {
          t.notOk(rows.length, 'fixDoughnuts is OK');
      })

      // delete empty polygons
      .then(function () {
        t.equal(typeof cleaner.deleteEmptyPolygons, 'function', 'Cleaner.deleteEmptyPolygons() exists');
        return cleaner.deleteEmptyPolygons();
      }).then(function () {
        return dbClient('dxf_features')
        .select('id')
        .whereRaw('ST_Area(geom) <= 1')
        .andWhere('type', 'polygon');
      }).then(function (rows) {
        t.notOk(rows.length, 'deleteEmptyPolygons is OK');
      })

      // label polygons
      .then(function () {
        t.equal(typeof cleaner.labelPolygons, 'function', 'Cleaner.labelPolygons() exists');
        return cleaner.labelPolygons();
      }).then(function () {
        return dbClient('dxf_features')
        .select('name')
        .whereIn('id', [1,44])
        .orderBy('id');
      }).then(function (rows) {
        t.equal(rows[0].name, '001,004,003', 'label correctly assigned');
        t.equal(rows[1].name, null, 'no label correctly assigned');
      })

      // fix doughnut labels
      .then(function () {
        t.equal(typeof cleaner.fixDoughnutLabels, 'function', 'Cleaner.fixDoughnutLabels() exists');
        return cleaner.fixDoughnutLabels();
      }).then(function () {
        return dbClient('dxf_features')
        .select('id', 'name')
        .whereIn('id', [12, 13, 15, 16])
        .orderBy('id');
      })
      .then(function (rows) {
        t.equal(rows[0].name, '007', 'doughnut label correctly assigned');
        t.equal(rows[1].name, null, 'no doughnut label correctly assigned');
        t.equal(rows[2].name, '008', 'doughnut label correctly added');
        t.equal(rows[3].name, null, 'doughnut label correctly removed');
      })

      // flag polygons
      .then(function () {
        t.equal(typeof cleaner.flagPolygons, 'function', 'Cleaner.flagPolygons() exists');
        return cleaner.flagPolygons();
      }).then(function () {
        return dbClient('dxf_features')
        .select('validations')
        .whereIn('id', [1,9,44])
        .orderBy('id');
      })
      .then(function (rows) {
        t.equal(rows[0].validations.hasLabel, true, 'hasLabel is OK');
        t.equal(rows[0].validations.hasManyLabels, true, 'hasManyLabels is OK');
        t.equal(rows[0].validations.hasSharedLabels, true, 'hasSharedLabels is OK');
        t.equal(rows[0].validations.isOverlapping, true, 'isOverlapping is OK');
        t.ok(rows[0].validations.overlappingPolygons.indexOf(18) > -1, 'overlappingPolygons is OK');
        t.equal(rows[2].validations.hasLabel, false, 'has no Label is OK');
        t.ok(rows[2].validations.compactness < 0.2, 'compactness is OK');
        t.equal(Math.round(rows[2].validations.area), 91, 'area is OK');
        t.equal(rows[1].validations.isValid, false, 'isValid is OK');
        t.equal(rows[1].validations.validReason, 'Self-intersection', 'validReason is OK');
      })

      // flag labels
      .then(function () {
        t.equal(typeof cleaner.flagLabels, 'function', 'Cleaner.flagLabels() exists');
        return cleaner.flagLabels();
      }).then(function () {
        return dbClient('dxf_features')
        .select('validations')
        .whereIn('id', [3,10,27])
        .orderBy('id');
      })
      .then(function (rows) {
        t.equal(rows[0].validations.isInPolygon, true, 'in polygon is OK');
        t.equal(rows[0].validations.isInManyPolygons, true, 'duplicate label detection is OK');
        t.equal(rows[1].validations.isInPolygon, false, 'outside of polygon is OK');
        t.equal(rows[2].validations.isInManyPolygons, false, 'single label is OK');
      })

      // check for single linestring
      .then(function () {
        return dbClient('dxf_features')
        .select('id')
        .whereRaw('GeometryType(geom) = \'LINESTRING\'');
      })
      .then(function (rows) {
        t.equals(rows.length, 1, 'only one linestring is OK');
      })

      // approve features
      .then(function () {
        t.equal(typeof cleaner.approvePolygons, 'function', 'Cleaner.approvePolygons() exists');
        return cleaner.approvePolygons();
      }).then(function () {
        return dbClient.schema.hasTable('approved_dxf_features');
      }).then(function (exists) {
        t.ok(exists, 'approved feature table created');
      }).then(function () {
        return dbClient('approved_dxf_features').count();
      }).then(function (data) {
        t.equals(data[0].count, '12', 'approved features OK');
      })

      // invalid geometries
      .then(function () {
        t.equal(typeof cleaner.getInvalidGeometryDetails, 'function', 'Cleaner.getInvalidGeometryDetails() exists');
        return cleaner.getInvalidGeometryDetails();
      }).then(function (data) {
        t.equal(data.type, 'FeatureCollection', 'invalid featurecollection OK');
        t.equal(data.features.length, 4, 'invalid features OK');
      })

      // overall results
      .then(function () {
        return cleaner.getGeoJson({approved: true});
      })
      .then(function (geojson) {
        var expected, found,
            pass = true;

        expected = [ '0,02,003', '001', '001,004,003', '005C', '007', '008', '010', '010A', '011', '011A', '012', '013' ];
        found = _.map(geojson.features, function (f) {
          return f.properties.name;
        }).sort();

        _.forEach(expected, function (e,i) {
          if (e !== found[i]) {
            pass = false;
            t.fail('Expected ' + e + ' and found ' + found[i] + ' in approved results.');
          }
        });
        if (pass) t.pass('overall results are OK');
      })

      // set version
      .then(function () {
        return cleaner.setVersion();
      })
      .then(function () {
        t.equal(cleaner._version, 1, 'Cleaner.setVersion() is OK');
      })



      // teardown
      .then(function () {
        t.equal(typeof cleaner.teardown, 'function', 'Cleaner.teardown() exists');
        return cleaner.teardown();
      })
      .then(function () {
        return dbClient.schema.hasTable('test_test_1_polygon');
      })
      .then(function (exists) {
        t.notOk(exists, 'temporary polygon view removed');
        return dbClient.schema.hasTable('test_test_1_label');
      })
      .then(function (exists) {
        t.notOk(exists, 'temporary label view removed');
      })

      // handle any error
      .catch(function (e) {
        console.error(e);
      });

    });


    test('cleaner full service', function (t) {
      var cleaner;

      t.plan(3);

      cleaner = Cleaner('test.dxf', 'test', 1);
      t.equal(typeof cleaner.fullService, 'function', 'Cleaner.fullService() exists');
      cleaner.fullService()
      .then(function (geojson) {
        t.equal(geojson.type, 'FeatureCollection', 'Cleaner.fullService() is OK');
        t.equal(geojson.features.length, 43, 'Cleaner.fullService() GeoJSON response is OK');
      })

        // handle any error
        .catch(function (e) {
          console.error(e);
        });
    });


    test('make valid', function (t) {
      var cleaner;

      t.plan(2);

      cleaner = Cleaner('test.dxf', 'test', 1);
      t.equal(typeof cleaner.makeValid, 'function', 'Cleaner.makeValid() exists');
      cleaner.makeValid()
      .then(function (data) {
        t.equal(data.rowCount, 4, 'Cleaner.makeValid() works');
        cleaner.teardown();
      })

      // handle any error
      .catch(function (e) {
        console.error(e);
      })

      // the end
      .finally(function () {
        dbClient.destroy();
      });

    });


  });
}


// import test data fixtures
function read () {
  var buf;
  while (buf = sqlStream.read()) {
    importDataFixture(buf);
  }
}

sqlStream = fs.createReadStream('./test/fixtures.sql', {encoding: 'utf8'});
sqlStream.on('readable', read);
