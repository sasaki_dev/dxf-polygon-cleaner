--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;


CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;
COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

DROP TABLE IF EXISTS approved_dxf_features CASCADE;
DROP TABLE IF EXISTS dxf_features CASCADE;
CREATE TABLE dxf_features (
    id integer NOT NULL,
    name character varying(255),
    filename character varying(255),
    version integer,
    project character varying(255),
    type character varying(255),
    validations json,
    geom geometry(Geometry,3857)
);

CREATE VIEW all_labels AS
 WITH latest AS (
         SELECT dxf_features.filename,
            max(dxf_features.version) AS version
           FROM dxf_features
          GROUP BY dxf_features.filename
        )
 SELECT v.id,
    v.name,
    v.filename,
    v.version,
    v.project,
    v.type,
    v.validations,
    v.geom
   FROM dxf_features v
  WHERE ((v.version = ( SELECT latest.version
           FROM latest
          WHERE ((latest.filename)::text = (v.filename)::text))) AND ((v.type)::text = 'label'::text))
  ORDER BY v.filename;

CREATE VIEW all_polygons AS
 WITH latest AS (
         SELECT dxf_features.filename,
            max(dxf_features.version) AS version
           FROM dxf_features
          GROUP BY dxf_features.filename
        )
 SELECT v.id,
    v.name,
    v.filename,
    v.version,
    v.project,
    v.type,
    v.validations,
    v.geom
   FROM dxf_features v
  WHERE ((v.version = ( SELECT latest.version
           FROM latest
          WHERE ((latest.filename)::text = (v.filename)::text))) AND ((v.type)::text = 'polygon'::text))
  ORDER BY v.filename;


CREATE SEQUENCE dxf_features_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE dxf_features_id_seq OWNED BY dxf_features.id;
ALTER TABLE ONLY dxf_features ALTER COLUMN id SET DEFAULT nextval('dxf_features_id_seq'::regclass);


INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (1, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F00000100000005000000D62C43562630F63F4EB8CFF0B2823040D62C43562630F63FCD13647AFB40204089069BAE3E992440CD13647AFB40204067069BAE3E9924404EB8CFF0B2823040D62C43562630F63F4EB8CFF0B2823040');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (2, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F0000010000000500000067069BAE3E9924404EB8CFF0B282304089069BAE3E992440CD13647AFB4020402A5C0520A9643140CD13647AFB4020402A5C0520A96431405DB8CFF0B282304067069BAE3E9924404EB8CFF0B2823040');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (3, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"001"}', '0101000020110F000024D79530F68D15402B14A4238A692740');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (4, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"0"}', '0101000020110F0000BBF3BAB5A2A22A40E71E2C24C6592A40');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (5, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"02"}', '0101000020110F0000C4217B0BBC322C40E1BACA1346532A40');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (6, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"004"}', '0101000020110F0000D62C43562630F63FCA13647AFB402040');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (7, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"003"}', '0101000020110F000084069BAE3E992440B313647AFB402040');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (8, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F0000010000000B000000D62C43562630F63FCA13647AFB402040D62C43562630F63FE874D8C0E486144076B42132D2A91D40E874D8C0E486144076B42132D2A91D4044B8F83C2701FB3FCAA3590A83C20F4044B8F83C2701FB3FCAA3590A83C20F40E874D8C0E4861440D62C43562630F63FE874D8C0E4861440D62C43562630F63F9589439127D8C3BFC20DEE420D4722409589439127D8C3BF89069BAE3E992440CD13647AFB402040D62C43562630F63FCA13647AFB402040');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (9, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F0000010000000700000089069BAE3E992440CD13647AFB402040C20DEE420D4722409589439127D8C3BF0056A241041F30409AE0CCC8813DE23F2A5C0520A9643140CD13647AFB40204089069BAE3E992440B313647AFB402040C20DEE420D4722409589439127D8C3BF89069BAE3E992440CD13647AFB402040');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (10, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"005A"}', '0101000020110F0000E1BCA13AB25F114042709C3690700740');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (11, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"006"}', '0101000020110F00001BDA9587F14A2A40F3C2386B8DBE1040');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (12, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F000001000000060000002932DD209ACB33406D0CE761274D2840503E8B80B6DF32408B82715B03441540E72D1D274B8C384033D6F88F3ABC0D406FE839F60DF93A403A7ADBB016EF204090201057F4BD38401C16C2AF3CC928402932DD209ACB33406D0CE761274D2840');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (13, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F000001000000050000004F740B7BF28534404F866642DAC026408DA6F2EDF6F0334002952E4E02501A401A56C0CDD70338400F308B96130C164045FE959312F03840E04062C8F91222404F740B7BF28534404F866642DAC02640');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (14, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"007"}', '0101000020110F00001EDE85864CA1364087AE8B9479B91340');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (15, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F00000100000006000000F98DD836389534403E570EC5393037401E9A869654A93340AA31F7EAA65A3040B189183DE95539409DD733CC5A822D403C44350CACC23B401E8E886C318133405A7C0B6D9287394016DCFB6B446E3740F98DD836389534403E570EC539303740');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (16, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F0000010000000500000019D00691904F354030144E35136A36405A02EE0395BA34404976A6A7A69D3140E8B1BBE375CD38400C9DBDF9AA8C3040125A91A9B0B939407AF14BF82213344019D00691904F354030144E35136A3640');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (17, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"008"}', '0101000020110F0000DE01FDDFB1BF3540F9AD74FA5E763240');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (18, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F000001000000060000002F316CD866F109C0A6446B5FA9DC32405DD6F3F36597FEBF37918CC7B3C52B403A80D609D91F0240E7B96336CD012E4029696D79411C0A40DFC3C7079A9D32402E057342C91BE03F80F7676F5DEE34402F316CD866F109C0A6446B5FA9DC3240');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (19, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"001"}', '0101000020110F0000560D3DB338BEFB3F8DEA3A36511F2F40');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (20, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"005"}', '0101000020110F0000083A1BC489E60D40D5D6C917CE371840');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (21, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F0000010000000A000000D62C43562630F63F9589439127D8C3BFD62C43562630F63F1465A838008408C0D252D457EC1218401A65A838008408C0D252D457EC121840543214B70D8515C06871BEA59A5FF8BF4E3214B70D8515C06871BEA59A5FF8BF32C47015532622C063EB443D43D32340DC2817FF8DFA1EC01D883460D53F224089B621062008C4BFD62C43562630F63F89B621062008C4BFD62C43562630F63F9589439127D8C3BF');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (22, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"009A"}', '0101000020110F0000C51F03733B9BFF3F83EE6D4CC8C212C0');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (23, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"009"}', '0101000020110F000072D1C5BF4223134041585F3C95B0FFBF');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (24, NULL, 'test.dxf', 1, 'test', 'unknown', '{}', '0102000020110F000002000000D62C43562630F63F1465A838008408C0D62C43562630F63F533214B70D8515C0');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (25, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F00000100000005000000C20DEE420D4722407189439127D8C3BF0056A241041F30409AE0CCC8813DE23FAE566727851134407A0C5C3113A11BC063EB443D43D32340DC2817FF8DFA1EC0C20DEE420D4722407189439127D8C3BF');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (26, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F00000100000005000000B24E352CF5802C40289E494020DA0CC0E96CA341791F2D40B0B8FA8D0A721DC0AE566727851134407A0C5C3113A11BC00C8B4B90CCEA31409C94832854A806C0B24E352CF5802C40289E494020DA0CC0');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (27, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"010"}', '0101000020110F000076C0D1404B1E2740DAFD660800C205C0');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (28, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"011"}', '0101000020110F0000BA708383EE282F40B0A3D4B26D8116C0');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (29, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F0000010000000500000063EB443D43D32340DC2817FF8DFA1EC066D8E522BBE32540FCBB501F4CAF31C06FFE94011E6436402CF08404892D2FC0AE566727851134407A0C5C3113A11BC063EB443D43D32340DC2817FF8DFA1EC0');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (30, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F00000100000005000000B07BC87CBA6A2C40C72492C4B8E626C02C8334E3A1342D40967FE7AFFB5F2FC00EA5DF0C51073540909BB137C42D2EC07EAA246C8CE03340287B520792A224C0B07BC87CBA6A2C40C72492C4B8E626C0');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (31, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"012"}', '0101000020110F0000B8BE58E719A92840D05A46018EBF23C0');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (32, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"013"}', '0101000020110F00001C2C5BEA77213040809E4C395A232BC0');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (33, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F0000010000000B000000A97614326A1A1EC0CA13647AFB402040A97614326A1A1EC0ED74D8C0E48614409F350E5686F2F7BFED74D8C0E48614409F350E5686F2F7BF4DB8F83C2701FB3FF86F784232C513C04DB8F83C2701FB3FF86F784232C513C0ED74D8C0E4861440A97614326A1A1EC0ED74D8C0E4861440A97614326A1A1EC07189439127D8C3BFD62C43562630F63F7189439127D8C3BFD62C43562630F63FCD13647AFB402040A97614326A1A1EC0CA13647AFB402040');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (34, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"005C"}', '0101000020110F0000B99E69CEFFF811C044709C3690700740');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (35, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"005B"}', '0101000020110F0000D9A497E52EB314C0D5D6C917CE371840');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (36, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F00000100000005000000F86F784232C513C0ED74D8C0E48614409F350E5686F2F7BFED74D8C0E48614409F350E5686F2F7BF4DB8F83C2701FB3FF86F784232C513C04DB8F83C2701FB3FF86F784232C513C0ED74D8C0E4861440');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (37, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F000001000000050000009EFBDBCEB5A431405C2E62242DCEE33FFD3667CAE7463A40C3E1521BAC84F63FAB372CB068393E4018F0A063984718C0BD648959C6EA3540B7B1802CFD041BC09EFBDBCEB5A431405C2E62242DCEE33F');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (38, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F000001000000050000005688DF1E5E6838406665D3A42A2706C072979629A0B738404F9C3FC08F181AC0AB372CB068393E4018F0A063984718C00A6C1019B0123C40B6B71A1ABDEAFFBF5688DF1E5E6838406665D3A42A2706C0');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (39, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"010A"}', '0101000020110F00001D2A1B5A34C33640318AE1D9141EFEBF');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (40, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"011A"}', '0101000020110F00002E66BCD802383940558719E5F22713C0');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (41, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F000001000000070000009EFBDBCEB5A431405C2E62242DCEE33FBD648959C6EA3540B7B1802CFD041BC072979629A0B738404F9C3FC08F181AC05688DF1E5E6838406665D3A42A2706C00A6C1019B0123C40B6B71A1ABDEAFFBFFD3667CAE7463A40C3E1521BAC84F63F9EFBDBCEB5A431405C2E62242DCEE33F');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (42, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"014"}', '0101000020110F0000EC213B955CE816C0F214FA78BAB12340');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (43, NULL, 'test.dxf', 1, 'test', 'label', '{"text":"006A"}', '0101000020110F0000FA618E07E2EC264045549C6B44FA0040');
INSERT INTO dxf_features (id, name, filename, version, project, type, validations, geom) VALUES (44, NULL, 'test.dxf', 1, 'test', 'polygon', '{}', '0103000020110F00000100000009000000B5E42FF10922F8BF7DCBB147FA0E24C099C4F5FAB1EE21407DCBB147FA0E24C097C4F5FAB1EE2140FCBB501F4CAF31C0AA7614326A1A1EC0F9BB501F4CAF31C0A97614326A1A1EC0DB9DE9787D96EEBF2DD7B6AA3C3D1CC0DB9DE9787D96EEBF2FD7B6AA3C3D1CC0979F30740C1D31C0BEE42FF10922F8BF979F30740C1D31C0B5E42FF10922F8BF7DCBB147FA0E24C0');


SELECT pg_catalog.setval('dxf_features_id_seq', 44, true);


ALTER TABLE ONLY dxf_features
    ADD CONSTRAINT dxf_features_pkey PRIMARY KEY (id);


REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--
