module.exports = {
  host      : process.env.DXF_TOOLS_DB_HOST || '127.0.0.1',
  port      : process.env.DXF_TOOLS_DB_PORT || 5432,
  user      : process.env.DXF_TOOLS_DB_USER || 'postgres',
  password  : process.env.DXF_TOOLS_DB_PASSWORD || '',
  database  : process.env.NODE_ENV === 'test' ? 'test' : process.env.DXF_TOOLS_DB_NAME
};
